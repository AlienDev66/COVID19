const connection = require('../database/connection');

module.exports ={

    async list (request, response) {
        const list_sintomas = await connection('sintomas').select('*');
        return response.json(list_sintomas);

    },

    async create (request, response) {
        const { id, title, sintoma } = request.body;

        await connection('sintomas').insert({
            title,
            sintoma
        })
        return response.json({ id });
    }

};