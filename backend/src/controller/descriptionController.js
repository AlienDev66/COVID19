const connection = require('../database/connection');

module.exports ={

    async list (request, response) {
        const description_list = await connection('description').select('*');
        return response.json(description_list);
    },

    async create (request, response) {
        const {id, title, description } = request.body;

        await connection('description').insert({
            title,
            description
        })

        return response.json({ id });
    }
};