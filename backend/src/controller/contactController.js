const connection = require('../database/connection');

module.exports ={

    async list (request, response) {
        const list_contacts = await connection('contact').select('*');
        return response.json(list_contacts);
    },

    async create (request, response) {
        const { id, Contacto } = request.body;

        await connection('contact').insert({
            Contacto
        })
        return response.json({ id });
    }


}