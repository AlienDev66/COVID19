const connection = require('../database/connection');

module.exports ={

    async list (request, response) {
        const list_prevencao = await connection('prevencao').select('*');
        return response.json(list_prevencao);
    },

    async create (request, response) {
        const { id, title, prevencao } = request.body;

        await connection('prevencao').insert({
            title,
            prevencao
        })
        return response.json({ id });
    }

}