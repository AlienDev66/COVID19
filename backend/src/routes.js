const express = require('express');
const descriptionController = require('./controller/descriptionController');
const sintomasController = require('./controller/simtomasController');
const prevencaoController = require('./controller/prevencaoController');
const contactController = require('./controller/contactController');

const routes= express.Router();

routes.post('/create_description', descriptionController.create);
routes.get('/list_description', descriptionController.list);

routes.post('/create_sintoma', sintomasController.create);
routes.get('/list_sintomas', sintomasController.list);

routes.post('/create_prevencao', prevencaoController.create);
routes.get('/list_prevencoes', prevencaoController.list);

routes.post('/create_contact', contactController.create);
routes.get('/list_contact', contactController.list);


module.exports = routes;