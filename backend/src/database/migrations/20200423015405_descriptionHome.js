
exports.up = function(knex) {
  return knex.schema.createTable('description', function (table){
    table.increments();
    table.string('title').notNullable();
    table.string('description').notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('description');
};
