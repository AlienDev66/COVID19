
exports.up = function(knex) {
  return knex.schema.createTable('prevencao', function(table){
    table.increments();
    table.string('title').notNullable();
    table.string('prevencao').notNullable();
  });
};

exports.down = function(knex) {
    return knex.schema.dropTable('prevencao');
};
